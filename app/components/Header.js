import React, { Component } from "react";

import "./Header.scss";

class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="container">
          <h1 className="logo">Header</h1>
          <ul className="menu">
            <li>
              <span onClick={() => console.log("login")}>login</span>
            </li>
            {/* <li><span>login</span></li> */}
          </ul>
        </div>
      </header>
    );
  }
}

export default Header;
