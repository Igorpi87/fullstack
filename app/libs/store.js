import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

import reducers from "./reducers";

const middleWares = [];

export default () => createStore(reducers, undefined, composeWithDevTools(applyMiddleware(...middleWares)));
