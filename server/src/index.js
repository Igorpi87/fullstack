import { port } from "./utils/config";
import { app, server } from "./app";

app.listen(port, () => console.log(`server started on port ${port}${server.graphqlPath}`));
